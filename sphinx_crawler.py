#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: adenbley
Script to grab all rst files from a sphinx source
"""

import os
import requests

rootdir = '/home/adenbley/Dropbox/code/sphinx/'

projectname = 'sqlalchemy_0.9'
rooturl = 'http://docs.sqlalchemy.org/en/rel_0_9/_sources/contents.txt'


def getrst(filepath, urlname):
    """This is a recursive function to get files from an rst file.
    It relies on the toc to be on the root level and read '.. toctree::'.
    """
    #download the url
    r = requests.get(urlname)
    #get the filename from the url
    filename = urlname[urlname.rfind('/')+1:]
    baseurl = urlname[:urlname.rfind('/')+1]
    ext = filename[filename.rfind('.'):]
    #make required directories if needed
    try:
        os.makedirs(filepath)
    except:
        pass
    #write the downloaded data to a local file
    open(os.path.join(filepath, filename), 'w').write(r.text)
    otherfiles = []
    infile = r.text.split('\n')
    intoc = False
    while len(infile):
        line = infile.pop(0)
        if ".. toctree::" in line:
            intoc = True
            continue
        if intoc and ((':maxdepth:' in line) 
                      or (0==len(line.strip()))
                      or (':glob:' in line)):
                pass
        elif (intoc and 
            line.startswith('   ')):
            otherfiles.append(line.strip())
        elif intoc:
            intoc = False
            continue
    for line in otherfiles:
        print "url:", baseurl + line + ext
        newurl = baseurl + line + ext
        if len(line.split('/')) == 1:
            getrst(filepath, newurl)
            #print "filepath:", filepath
        else:
            getrst(os.path.join(filepath, *(line.split('/')[:-1])), newurl)
            #print "filepath:", os.path.join(filepath, *(line.split('/')[:-1]))
            
filepath = os.path.join(rootdir, projectname)
getrst(filepath, rooturl)        
